![Logo](../img/SHAPEware-logo.png)

***
# How to install SHAPEware dependencies

## R

R is a widely used language for statistical computing. SHAPEware relies on it for plotting and statistical calculations.
The easiest way to install R depends on your operating system:

**Note**: If you are installing R from scratch, make sure you install at least one package following installation. This will set up
your directories for later package installation. You can do this through the terminal:

	>R
	install.packages('ggplot2')  # You will need this one anyway

Once this process completes successfully, you will be all set for installing SHAPEware dependencies.

#### MacOS

Download the latest version from [this page](http://cran.us.r-project.org/bin/macosx/) and double-click on the downloaded .pkg file.

#### Amazon Linux / CentOS

Use this terminal command:

	sudo yum install R

#### Ubuntu

In most cases, this should provide you with a reasonably up-to-date version of R.
	
	sudo apt-get install r-base r-base-dev

In case this doesn't work for you, follow the instructions [here](https://cloud.r-project.org/bin/linux/ubuntu/).

#### Debian

Follow the instructions [here](https://cloud.r-project.org/bin/linux/debian/).

***

## Conda

Conda is a package manager that allows easy downloading of dependencies and maintenance of isolated computing environments.

You can download the latest version from [here](https://conda.io/miniconda.html) (both MacOS and Linux versions).

Once you download it, you can run the executable by double-clicking or in a terminal window. For example,

	cd Downloads/
	./Miniconda3-latest-MacOSX-x86_64.sh 

***

## Python

You most likely already have Python, but if you don't, you can download an installer from [here](https://anaconda.org/anaconda/python).

On MacOS, it is also part of [Xcode](https://developer.apple.com/xcode/).

***

## Git

You most likely already have Git, but if you don't, you can download it from [here](https://git-scm.com/downloads).

On MacOS, it is also part of [Xcode](https://developer.apple.com/xcode/).




