
# Makes a reactivity plot. Designed to be used with snakemake.

suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(dplyr))
suppressPackageStartupMessages(library(reshape2))
suppressPackageStartupMessages(library(jsonlite))

deigan_normalize <- function(reactivity.values, seq.length) {

	# Calculate reactivity using the method proposed by Deigan et al, PNAS, (2009)
	# This is done after excluding the values that are either >1.5 IQR or in the original 5% (if length of sequence <= 100 )
	# or the original 10%.

	limit.iqr <- 1.5 * IQR(reactivity.values)

	if (max(seq.length) <= 100)
		percent.thresh <- 0.05
	else
		percent.thresh <- 0.1

	num.values <- as.integer(percent.thresh * max(seq.length))
	num.values.10p <- as.integer(0.1 * max(seq.length))
	limit.percent <- reactivity.values[order(-reactivity.values)][num.values]

	limit <- max(limit.iqr, limit.percent)

	kept.values <- reactivity.values[reactivity.values < limit]

	# Now take the mean of top 10% of the remaining values
	norm.factor <- mean(kept.values[order(-kept.values)][1:num.values.10p])

	return(norm.factor)

}


plot_reactivities <- function(shape.file, noshape.file, denat.file, reactivity.plot.file, reactivity.csv.file, 
							  discard.plot.file, mutation.plot.file, treatment.json, sample.wildcards, ref.json,
							  mask.json, min.coverage, max.mut.rate) {

	treatment.dict <- fromJSON(treatment.json)
	ref.dict <- fromJSON(ref.json)
	mask.dict <- fromJSON(mask.json)

	arr.palette <- c('#fa8d29', '#875696',  '#31859c',  '#41395f')

	shape.df <- read.table(shape.file)
	shape.df$Type <- '+SHAPE'


	noshape.df <- read.table(noshape.file)
	noshape.df$Type <- '-SHAPE'

	denat.df <- read.table(denat.file)
	denat.df$Type <- 'Denatured'

	plot.df <- rbind(shape.df, noshape.df, denat.df)

	names(plot.df) <- c('Sequence', 'Position', 'Base', 'Coverage', 'Mutation.Rate', 'Amb.Mutation.Rate', 'Del.Discard.Count', 'Ins.Discard.Count', 'Type')

	plot.df$Type <- factor(plot.df$Type, levels = c('+SHAPE', '-SHAPE', 'Denatured'))

	# We assigned the treatment status based on the +SHAPE sample. This is an assumption.

	sample.name <- sample.wildcards[['sample1']][1]
	
	plot.df$Treatment <- treatment.dict[sample.name][[1]][1]

	ref.seqs <- ref.dict[[sample.name]]

	plot.df <- plot.df[plot.df$Sequence %in% ref.seqs,]

	

	max.pos <- max(as.integer(plot.df$Position))
	plot.height <- 4 * length(unique(plot.df$Sequence))


	##########
	# Make plot showing position of discarded variants
	##########

	discard.df <- plot.df[,c('Sequence', 'Treatment', 'Position', 'Base', 'Type', 'Del.Discard.Count', 'Ins.Discard.Count')]
	discard.df <- melt(discard.df, id.vars = c('Sequence', 'Treatment', 'Position', 'Base', 'Type'))

	# Make plot showing where variants were discarded

	p <- ggplot(discard.df, aes(x = Position, y = value, fill = variable)) +
	geom_bar(stat = 'identity') +
	geom_text(aes(x = Position, label = Base), y=-100, size=2) + 
	scale_x_continuous(breaks = c(1,seq(10, max.pos, 10))) +
	scale_fill_manual(labels = c('Del', 'Ins'), name = 'Type', values = arr.palette) +
	ylab('# of variants discarded') + 
	facet_wrap(~ Sequence + Type, ncol = 1, scales = 'free_x') +
	theme_bw() 

	suppressWarnings(ggsave(discard.plot.file, p, width = max.pos / 8, height = 7 * length(unique(discard.df$Sequence)), limitsize=FALSE))

	##########
	# Plot mutation rate for each sample
	##########	

	p <- ggplot(plot.df, aes(x = Position, y = 100 * Mutation.Rate, color = Type)) +
	geom_errorbar(aes(x = Position, ymin = 100 * Mutation.Rate, ymax = 100 * Amb.Mutation.Rate, color = Type), width = 0.25, alpha = 0.5) +
	geom_line() + 
	geom_point() +
	geom_text(aes(x = Position, label = Base), y=-0.1, size=2, color = 'black') +
	scale_color_manual(values = arr.palette) + 
	facet_wrap(~ Sequence, ncol = 1, scales='free_x') +
	theme_bw() +
	ylab('Mutation rate (%)')

	suppressWarnings(ggsave(mutation.plot.file, p, width = max.pos/8 + 1, height = 4 * length(unique(plot.df$Sequence)), limitsize=FALSE))


	##########
	# Make reactivity plots
	##########

	# Add pseudocounts to avoid division by zero or bogus values caused by noise around very small denominators

	plot.df$Mutation.Rate <- plot.df$Mutation.Rate + 1e-5
	plot.df$Amb.Mutation.Rate <- plot.df$Amb.Mutation.Rate + 1e-5

	plot.df <- plot.df %>% group_by(Sequence, Treatment, Position, Base) %>% 
			   summarize(Reactivity = (Mutation.Rate[Type == '+SHAPE'] - Mutation.Rate[Type == '-SHAPE'])/ Mutation.Rate[Type == 'Denatured'],
			   	         Amb.Reactivity = (Amb.Mutation.Rate[Type == '+SHAPE'] - Amb.Mutation.Rate[Type == '-SHAPE'])/ Amb.Mutation.Rate[Type == 'Denatured'],
			   	         WCS.Coverage = min(Coverage[Type == '+SHAPE'], Coverage[Type == '-SHAPE'], Coverage[Type == 'Denatured']),
			   	         BG.Rate = max(Amb.Mutation.Rate[Type == '+SHAPE'], Amb.Mutation.Rate[Type == '-SHAPE'], Amb.Mutation.Rate[Type == '-Denatured']))


	plot.df$Raw.Reactivity <- plot.df$Reactivity

	# Classify positions according to whether they pass or fail the coverage filter

	plot.df$Coverage.Filter <- 'Fail'
	plot.df$Coverage.Filter[plot.df$WCS.Coverage >= min.coverage] <- 'Pass'

	plot.df$Mutation.Filter <- 'Fail'
	plot.df$Mutation.Filter[plot.df$BG.Rate <= max.mut.rate] <- 'Pass'	

	plot.df$Reactivity.Bin <- 'Low'

	for (i in 1:length(ref.seqs)) {
		ref.seq <- ref.seqs[i]

		blackout.mask <- mask.dict[ref.seq][[1]] + 1

		plot.df$Reactivity.Bin[plot.df$Sequence == ref.seq & plot.df$Position %in% blackout.mask] <- 'Masked'

		max.pos <- max(plot.df$Position[plot.df$Sequence == ref.seq])

		curr.seq.mask <- plot.df$Sequence == ref.seq & plot.df$Coverage.Filter == 'Pass' & 
						 plot.df$Mutation.Filter == 'Pass' & plot.df$Reactivity.Bin != 'Masked'

		norm.factor <- deigan_normalize(plot.df$Reactivity[curr.seq.mask], max.pos)
		
		plot.df$Reactivity[curr.seq.mask] <- plot.df$Reactivity[curr.seq.mask] / norm.factor
		plot.df$Amb.Reactivity[curr.seq.mask] <- plot.df$Amb.Reactivity[curr.seq.mask] / norm.factor

	}


	plot.df$Reactivity.Bin[plot.df$Reactivity > 0.3 & plot.df$Reactivity.Bin != 'Masked'] <- 'Mid'
	plot.df$Reactivity.Bin[plot.df$Reactivity > 0.7 & plot.df$Reactivity.Bin != 'Masked'] <- 'High'

	# Include positions that were manually masked
	
	plot.df$Reactivity.Bin <- factor(plot.df$Reactivity.Bin, levels = c('High', 'Mid', 'Low', 'Masked'))

	plot.df$Min.Reactivity <- apply(plot.df[, c('Reactivity', 'Amb.Reactivity')], 1, min) 
	plot.df$Max.Reactivity <- apply(plot.df[, c('Reactivity', 'Amb.Reactivity')], 1, max) 

	plot.df$Base <- sub('T', 'U', plot.df$Base)

	filtered.df <- plot.df[plot.df$Coverage.Filter == 'Pass' & plot.df$Mutation.Filter == 'Pass',]

	plot.height <- 4 * length(unique(filtered.df$Sequence))

	if (nrow(filtered.df) == 0)
	{
		stop('No positions have acceptable coverage.')
	}

	p <- ggplot(filtered.df, aes(x = Position, y = Reactivity, fill = Reactivity.Bin)) +
	geom_text(aes(x = Position, label = Base), y=-0.04, size=2) + 
	geom_bar(stat = 'identity') +
	geom_errorbar(aes(x = Position, ymin = Min.Reactivity, ymax = Max.Reactivity), width = 0.1) +
	scale_x_continuous(breaks = c(1,seq(10, max.pos, 10))) +
	scale_fill_manual(values = c('red', 'orange', 'black', 'gray'), name = 'Bin') +
	facet_wrap(~ Sequence, ncol = 1) +
	ylab('SHAPE reactivity') +
	theme_bw()

	suppressWarnings(ggsave(reactivity.plot.file, p, width = max.pos / 8, height = plot.height, limitsize=FALSE))

	write.csv(plot.df, reactivity.csv.file, row.names=FALSE, quote=FALSE)


}

plot_reactivities(snakemake@input[[1]], snakemake@input[[2]], snakemake@input[[3]], snakemake@output[[1]], snakemake@output[[2]],
		          snakemake@output[[3]], snakemake@output[[4]], snakemake@config[['treatment_dict']], snakemake@wildcards, 
		          snakemake@config[['ref_seq_dict']], snakemake@config[['mask_dict']], as.integer(snakemake@config[['min_coverage']]),
		          as.numeric(snakemake@config[['max_mut_rate']]))

# args <- commandArgs(TRUE)
# plot_reactivities(args[1], args[2], args[3], args[4], args[5])
