#!/usr/bin/env python3

""" 
Contains functions that compare 2D folding predictions vs. a reference 2D structure.

The main function is calc_stats, which calculates various traditional statistics, such as the positive-predictive value
(PPV), the sensitivity and the Matthews Correlation Coefficient. 

Part of the SHAPEware suite.

Author: Luis Barrera

"""

def get_pairs_as_set(dotb_string):
    """Deconstruct a dot bracket string (potentially with pseudoknots) into pairs of positions."""

    from collections import defaultdict

    pairs = []
    db_partners = {'(': ')', '{': '}', '[': ']'}
    rev_db_partners = {v: k for k, v in db_partners.items()} 

    left_queue = defaultdict(list)
    for pos, db_char in enumerate(dotb_string):
            if db_char == '.':
                continue
            elif db_char in db_partners:
                    left_queue[db_char].append(pos)
            else: 
                left_pos = left_queue[rev_db_partners[db_char]].pop()
                right_pos = pos

                pairs.append((left_pos, right_pos))

    return set(pairs)


def calc_stats(pred_dotb, obs_dotb, print_false_pairs=False):
    """Calculates relevant statistics for folding prediction accuracy given observed and predicted dot-bracket strings."""

    from numpy import sqrt

    if len(pred_dotb) != len(obs_dotb):
        print(pred_dotb)
        print(obs_dotb)
        raise Exception('Length mismatch between dot-bracket strings.')

    pred_pairs = get_pairs_as_set(pred_dotb)
    obs_pairs = get_pairs_as_set(obs_dotb)

    if print_false_pairs:
        print('====================================')
        print(pred_dotb)
        print(obs_dotb)

        print('FP:')
        for x, y in sorted(list(pred_pairs - obs_pairs)):
            print(x + 1, y + 1)

        print('FN:')
        for x, y in sorted(list(obs_pairs - pred_pairs)):
            print(x + 1, y + 1)


    tp = float(len(pred_pairs & obs_pairs))
    fp = float(len(pred_pairs - obs_pairs))
    fn = float(len(obs_pairs - pred_pairs))

    # The set of all possible pairs is of cardinality L * (L - 1) / 2, so whatever is left must be true negatives.

    tn = float((len(pred_dotb) * (len(pred_dotb) - 1) // 2)  - tp - fp - fn)

    if tp + fp != 0:
        ppv = tp / (tp + fp)
    else:
        ppv = 'NA'

    if tp + fn != 0:
        sens = tp / (tp + fn)
    else:
        sens = 'NA'

    try:
        mcc_exact = (tp * tn - fp * fn) / sqrt((tn + fn) * (tn + fp) *  (tp + fn) * (tp + fp))
    except:
        mcc_exact = 'NA'


    return {'TP': tp, 'FP': fp, 'FN': fn, 'TN': tn, 'PPV': ppv, 'Sensitivity': sens, 'MCC': mcc_exact}



def calc_fold_statistics(ref_struct_file, obs_file, out_csv_file):

    import pandas as pd

    pred_df = pd.read_table(obs_file)

    ref_seq_dict = {}

    for i, line in enumerate(open(ref_struct_file)):
        if line[0] == '>':
            seq_name = line[1:].strip()

        if (i - 2) % 3 == 0:
            ref_seq_dict[seq_name] = line.strip()


    new_df_rows = []
    for _, row in pred_df.iterrows():

        stats = calc_stats(row['mfe_db'], ref_seq_dict[row['seq_name']])

        row['ref_struct'] = ref_seq_dict[row['seq_name']]
        row['PPV'] = stats['PPV']
        row['Sensitivity'] = stats['Sensitivity']
        row['MCC'] = stats['MCC']
        

        new_df_rows.append(row)


    out_df = pd.DataFrame(new_df_rows)

    out_df.to_csv(out_csv_file, index=False)


if __name__ == '__main__':
    calc_fold_statistics(snakemake.input[0], snakemake.input[1], snakemake.output[0])


